// SPDX-License-Identifier: BSD-3-Clause

#ifndef _MTIME_CALENDAR_TEST_H
#define _MTIME_CALENDAR_TEST_H

#include <check.h>
#include "mtime_calendar.h"

void add_mtime_calendar_test_to_suite(Suite* suite);

#endif
